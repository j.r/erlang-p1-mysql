erlang-p1-mysql (1.0.21-1) unstable; urgency=medium

  * New upstream version 1.0.21

 -- Julian Ribbeck <j.r@jugendhacker.de>  Wed, 25 Jan 2023 16:47:56 +0000

erlang-p1-mysql (1.0.20-1) unstable; urgency=medium

  * New upstream version 1.0.20

 -- Philipp Huebner <debalance@debian.org>  Wed, 02 Nov 2022 14:39:38 +0100

erlang-p1-mysql (1.0.19-3) unstable; urgency=medium

  * Updated Standards-Version: 4.6.1 (no changes needed)
  * Updated Erlang dependencies
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Thu, 02 Jun 2022 10:19:38 +0200

erlang-p1-mysql (1.0.19-2) unstable; urgency=medium

  * Updated debian/watch

 -- Philipp Huebner <debalance@debian.org>  Sat, 18 Dec 2021 12:46:27 +0100

erlang-p1-mysql (1.0.19-1) unstable; urgency=medium

  * New upstream version 1.0.19
  * Updated Standards-Version: 4.6.0 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sun, 29 Aug 2021 21:07:04 +0200

erlang-p1-mysql (1.0.17-3) unstable; urgency=medium

  * Corrected Multi-Arch setting to "allowed"

 -- Philipp Huebner <debalance@debian.org>  Sun, 31 Jan 2021 18:33:28 +0100

erlang-p1-mysql (1.0.17-2) unstable; urgency=medium

  * Added 'Multi-Arch: same' in debian/control
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Sat, 30 Jan 2021 16:36:45 +0100

erlang-p1-mysql (1.0.17-1) unstable; urgency=medium

  * New upstream version 1.0.17
  * Updated Standards-Version: 4.5.1 (no changes needed)
  * Updated version of debian/watch: 4
  * Updated debhelper compat version: 13
  * Updated lintian overrides
  * Added debian/erlang-p1-mysql.install

 -- Philipp Huebner <debalance@debian.org>  Fri, 25 Dec 2020 23:02:21 +0100

erlang-p1-mysql (1.0.16-1) unstable; urgency=medium

  * New upstream version 1.0.16

 -- Philipp Huebner <debalance@debian.org>  Sun, 02 Aug 2020 16:45:58 +0200

erlang-p1-mysql (1.0.15-1) unstable; urgency=medium

  * New upstream version 1.0.15

 -- Philipp Huebner <debalance@debian.org>  Thu, 30 Apr 2020 09:25:18 +0200

erlang-p1-mysql (1.0.14-1) unstable; urgency=medium

  * New upstream version 1.0.14

 -- Philipp Huebner <debalance@debian.org>  Fri, 17 Apr 2020 13:27:37 +0200

erlang-p1-mysql (1.0.13-1) unstable; urgency=medium

  * New upstream version 1.0.13

 -- Philipp Huebner <debalance@debian.org>  Tue, 17 Mar 2020 17:09:31 +0100

erlang-p1-mysql (1.0.12-1) unstable; urgency=medium

  * New upstream version 1.0.12
  * Fixed typo in long package description.
  * Set 'Rules-Requires-Root: no' in debian/control
  * Updated Standards-Version: 4.5.0 (no changes needed)
  * Updated years in debian/copyright
  * Rules-Requires-Root: no

 -- Philipp Huebner <debalance@debian.org>  Tue, 04 Feb 2020 22:38:32 +0100

erlang-p1-mysql (1.0.11-1) unstable; urgency=medium

  * New upstream version 1.0.11
  * Updated Erlang dependencies
  * Updated Standards-Version: 4.4.0 (no changes needed)
  * debian/rules: export ERL_COMPILER_OPTIONS=deterministic
  * Updated debhelper compat version: 12

 -- Philipp Huebner <debalance@debian.org>  Wed, 24 Jul 2019 12:06:24 +0200

erlang-p1-mysql (1.0.8-1) unstable; urgency=medium

  * New upstream version 1.0.8
  * Updated Standards-Version: 4.3.0 (no changes needed)
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Tue, 01 Jan 2019 22:41:56 +0100

erlang-p1-mysql (1.0.7-1) unstable; urgency=medium

  * New upstream version 1.0.7
  * Enabled DH_VERBOSE in debian/rules
  * Updated Standards-Version: 4.2.1 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sun, 07 Oct 2018 14:57:37 +0200

erlang-p1-mysql (1.0.6-1) unstable; urgency=medium

  * New upstream version 1.0.6
  * Added debian/upstream/metadata
  * Updated Standards-Version: 4.1.4 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Wed, 04 Jul 2018 16:24:00 +0200

erlang-p1-mysql (1.0.5-1) unstable; urgency=medium

  * New upstream version 1.0.5
  * Use secure copyright format uri in debian/copyright
  * Switched to debhelper 11

 -- Philipp Huebner <debalance@debian.org>  Tue, 27 Mar 2018 22:49:22 +0200

erlang-p1-mysql (1.0.4-2) unstable; urgency=medium

  * Set Maintainer: Ejabberd Packaging Team <ejabberd@packages.debian.org>
  * Set Uploaders: Philipp Huebner <debalance@debian.org>
  * Updated Vcs-* fields in debian/control for salsa.debian.org
  * Updated years in debian/copyright
  * Updated Standards-Version: 4.1.3 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Thu, 04 Jan 2018 11:37:51 +0100

erlang-p1-mysql (1.0.4-1) unstable; urgency=medium

  * New upstream version 1.0.4
  * (Build-)Depend on erlang-base (>= 1:19.2)
  * Updated Standards-Version: 4.1.1 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Mon, 27 Nov 2017 20:01:41 +0100

erlang-p1-mysql (1.0.3-1) unstable; urgency=medium

  * New upstream version 1.0.3
  * Updated Standards-Version: 4.0.0 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Tue, 11 Jul 2017 15:43:12 +0200

erlang-p1-mysql (1.0.2-1~exp1) experimental; urgency=medium

  * New upstream version 1.0.2
  * Updated debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Tue, 11 Jul 2017 15:37:38 +0200

erlang-p1-mysql (1.0.1-4) unstable; urgency=medium

  * Added erlang-base to Build-Depends

 -- Philipp Huebner <debalance@debian.org>  Wed, 05 Oct 2016 15:57:41 +0200

erlang-p1-mysql (1.0.1-3) unstable; urgency=medium

  * Build-Depend on erlang-crypto (fixes FTBFS)

 -- Philipp Huebner <debalance@debian.org>  Wed, 07 Sep 2016 12:13:54 +0200

erlang-p1-mysql (1.0.1-2) unstable; urgency=medium

  * Improved debian/watch
  * Updated Standards-Version: 3.9.8 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sun, 26 Jun 2016 14:27:01 +0200

erlang-p1-mysql (1.0.1-1) unstable; urgency=medium

  * ProcessOne renamed mysql to p1_mysql
  * Imported Upstream version 1.0.1
  * Updated Standards-Version: 3.9.7 (no changes needed)
  * Updated Vcs-* fields in debian/control
  * Updated debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Wed, 17 Feb 2016 19:51:28 +0100

erlang-p1-mysql (1.0.0-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0
  * Updated debian/copyright
  * Updated debian/watch

 -- Philipp Huebner <debalance@debian.org>  Sat, 16 Jan 2016 11:45:38 +0100

erlang-p1-mysql (0.2015.09.29-1) unstable; urgency=medium

  * Imported Upstream version 0.2015.09.29

 -- Philipp Huebner <debalance@debian.org>  Sun, 04 Oct 2015 10:18:36 +0200

erlang-p1-mysql (0.2015.07.30-1) unstable; urgency=medium

  * Imported Upstream version 0.2015.07.30
  * Enabled eunit

 -- Philipp Huebner <debalance@debian.org>  Tue, 18 Aug 2015 07:37:43 +0200

erlang-p1-mysql (0.2015.02.04-1) unstable; urgency=medium

  * Imported Upstream version 0.2015.02.04
  * Updated Standards-Version: 3.9.6 (no changes needed)
  * Updated years in debian/copyright
  * Added Vcs links to debian/control

 -- Philipp Huebner <debalance@debian.org>  Sat, 02 May 2015 15:04:11 +0200

erlang-p1-mysql (0.2014.03.10-2) unstable; urgency=medium

  * Add correct Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sat, 10 May 2014 12:38:40 +0200

erlang-p1-mysql (0.2014.03.10-1) unstable; urgency=low

  * Initial release, see #744885 for background

 -- Philipp Huebner <debalance@debian.org>  Sun, 13 Apr 2014 14:38:02 +0200
